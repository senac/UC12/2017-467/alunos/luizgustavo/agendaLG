/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.agenda.dao;

import br.com.senac.agenda.dao.Conexao;
import br.com.senac.agenda.dao.DAO;
import br.com.senac.agenda.model.Contato;
import br.com.senac.agenda.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala302b
 */
public class ContatoDAO extends DAO<Contato>{

    @Override
    public void salvar(Contato contato) {
         Connection connection = null;
        try {
            String query;
            if (contato.getCodigo()== 0) {
                query = "INSERT INTO contato(nome , telefone , celular , fax , cep , endereco , numero , bairro , cidade , uf , email) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ;";
            } else {
                query = "UPDATE contato SET nome = ? ,  telefone = ? , celular = ? , fax = ? , cep = ? , endereco = ? , numero = ? , bairro = ? , cidade = ? , uf = ? , email = ? , where codigo = ?  ; ";
            }
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query,
                    PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, contato.getNome());
            ps.setString(2, contato.getTelefone());
            ps.setString(3, contato.getCelular());
            ps.setString(4, contato.getFax());
            ps.setString(5, contato.getCep());
            ps.setString(6, contato.getEndereco());
            ps.setString(7, contato.getNumero());
            ps.setString(8, contato.getBairro());
            ps.setString(9, contato.getCidade());
            ps.setString(10, contato.getUf());
            ps.setString(11, contato.getEmail());
            
            if (contato.getCodigo()== 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                contato.setCodigo(rs.getInt(1));
            } else {
                ps.setInt(12, contato.getCodigo());
                ps.executeUpdate();
            }

        } catch (Exception ex) {
          
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao....");
            }
        }

    }

    @Override
    public void deletar(Contato contato) {
         String query = "DELETE FROM contato WHERE id = ?";
        Connection connection = null;

        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, contato.getCodigo());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Erro ao deletar registro...");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao....");
            }
        }

    }

    @Override
    public List<Contato> listar() {
        String query = "SELECT * FROM contato;";
        List<Contato> lista = new ArrayList<>();
        Connection connection = null;
        try {
            connection = Conexao.getConnection(); /// abriu conexao com o banco 
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query); // executa a query e retorna uma "tabela"
            while (rs.next()) {
                Contato contato = new Contato();
                contato.setCodigo(rs.getInt("codigo"));
                contato.setNome(rs.getString("nome"));
                contato.setTelefone(rs.getString("telefone"));
                contato.setCelular(rs.getString("celular"));
                contato.setFax(rs.getString("fax"));
                contato.setCep(rs.getString("cep"));
                contato.setEndereco(rs.getString("endereco"));
                contato.setNumero(rs.getString("numero"));
                contato.setBairro(rs.getString("bairro"));
                contato.setCidade(rs.getString("cidade"));
                contato.setUf(rs.getString("uf"));
                contato.setEmail(rs.getString("email"));
                
                lista.add(contato);

            }

        } catch (Exception ex) {
            System.out.println("Ocorreu um erro ao fazer a consulta....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao....");
            }
        }

        return lista;
    }

    @Override
    public Contato get(int id) {
        Contato contato = null;
        Connection connection = null;
        String query = "SELECT * FROM contato where codigo = ? ; ";
        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                contato = new Contato();
                contato.setCodigo(rs.getInt("codigo"));
                contato.setNome(rs.getString("nome"));
                contato.setTelefone(rs.getString("telefone"));
                contato.setCelular(rs.getString("celular"));
                contato.setFax(rs.getString("fax"));
                contato.setCep(rs.getString("cep"));
                contato.setEndereco(rs.getString("endereco"));
                contato.setBairro(rs.getString("numero"));
                contato.setUf(rs.getString("cidade"));
                contato.setEmail(rs.getString("email"));
                
            }

        } catch (Exception ex) {
            System.out.println("Erro ao executar a consulta...");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao....");
            }
        }

        return contato;
    }
    }

   
    
    

